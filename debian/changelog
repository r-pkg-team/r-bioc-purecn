r-bioc-purecn (2.12.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 14 Jan 2025 08:56:53 +0100

r-bioc-purecn (2.12.0+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 30 Nov 2024 23:26:45 +0100

r-bioc-purecn (2.10.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:38:44 +0200

r-bioc-purecn (2.10.0+dfsg-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: depend on bioc-3.19+ versions of r-bioc-* packages

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 22 Jul 2024 11:44:06 +0200

r-bioc-purecn (2.10.0+dfsg-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 18:42:52 +0200

r-bioc-purecn (2.8.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 11:44:52 +0100

r-bioc-purecn (2.6.4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 21:55:12 +0200

r-bioc-purecn (2.4.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 22 Nov 2022 07:36:28 +0100

r-bioc-purecn (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 07:48:45 +0200

r-bioc-purecn (2.0.2+dfsg-2) unstable; urgency=medium

  * Add debian/tests/autopkgtest-pkg-r.conf

 -- Andreas Tille <tille@debian.org>  Tue, 08 Mar 2022 17:34:41 +0100

r-bioc-purecn (2.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 08 Mar 2022 12:37:07 +0100

r-bioc-purecn (2.0.1+dfsg-1) unstable; urgency=medium

  * Allow failure for piuparts
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 26 Nov 2021 14:11:47 +0100

r-bioc-purecn (1.22.2+dfsg-1) unstable; urgency=medium

  * Prepare for proper autopkgtest-pkg-r usage
  * Fix permissions
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-bioc-genomicranges,
      r-bioc-iranges and r-bioc-variantannotation.

 -- Andreas Tille <tille@debian.org>  Tue, 14 Sep 2021 20:56:01 +0200

r-bioc-purecn (1.20.0+dfsg-3) unstable; urgency=medium

  * Deactivate Testsuite: autopkgtest-pkg-r
    Closes: #981820

 -- Andreas Tille <tille@debian.org>  Sat, 06 Feb 2021 07:59:48 +0100

r-bioc-purecn (1.20.0+dfsg-2) unstable; urgency=medium

  [ Steffen Moeller ]
  * Smallish lintian quirks I only saw after upload. Sorry.

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Exclude test needing not packaged data set TxDb.Hsapiens.UCSC.hg19.knownGene
  * Exclude more tests that are failing due to missing data

 -- Andreas Tille <tille@debian.org>  Mon, 14 Dec 2020 17:52:18 +0100

r-bioc-purecn (1.20.0+dfsg-1) unstable; urgency=medium

  * Initial release (closes: #976789)

 -- Andreas Tille <tille@debian.org>  Fri, 20 Nov 2020 20:40:17 +0100
